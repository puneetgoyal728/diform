import cv2
import sys
import numpy as np

SCALE_FACTOR = 1.0

def displayImg(image, name):
	" Displays 'image' in the window with 'name' "
	cv2.namedWindow(name, cv2.WINDOW_NORMAL)
	cv2.imshow(name, image)


def destroyImg(name):
	" Destroys image window with 'name' "	
	if cv2.waitKey(0) == ord('a'):
		cv2.destroyWindow(name)


def rectangleResize(tol, boundingCords):
	# USAGE : rectangleResize(0.20,[0,0,img.shape[1],img.shape[0]])

	w = boundingCords[2]
	h = boundingCords[3]
	x1 = int(boundingCords[0] + (w*tol/2))
	if x1<boundingCords[0]:
		x1 = boundingCords[0]
	
	x2 = int(boundingCords[2] - (w*tol/2))
	if x2>boundingCords[2]:
		x2 = boundingCords[2]

	y1 = int(boundingCords[1] + (h*tol/2))
	if y1<boundingCords[1]:
		y1=boundingCords[1]
	
	y2 = int(boundingCords[3] - (h*tol/3))
	if y2>boundingCords[3]:
		y2 = boundingCords[3]
	return [x1, y1, x2, y2]	



def compute_skew(img):
	" Calculates skew in degree of a given img"
	status = False
	img = cv2.adaptiveThreshold(img, 255, cv2.ADAPTIVE_THRESH_GAUSSIAN_C, cv2.THRESH_BINARY, 3, 9)
	imgHeight, imgWidth = img.shape[:2]
	img = (255 - img)

	cordinates = rectangleResize(0.10,[0,0,img.shape[1],img.shape[0]])
	image = img[cordinates[1]:cordinates[3], cordinates[0]:cordinates[2]]
	# displayImg( image, "image")
	# destroyImg("image")
	lines = cv2.HoughLinesP(image, 1, np.pi/180, 100, minLineLength = (2*imgWidth/3), maxLineGap = 600)
	# print "lines   : ", lines
	count, angl = 0, 0.0
	if (lines is None) or (len(lines) <= 0) or (len(lines[0]) <= 0) :
		return angl, status;
	status = True	
	lines = lines[0]
	
	dispLines = np.zeros((imgHeight, imgWidth, 3), np.uint8)

	for l in lines:
		cv2.line(dispLines, (l[0], l[1]), (l[2], l[3]), (255, 0, 0))
		tempAngle = cv2.fastAtan2(l[3]-l[1], l[2]-l[0])
		if tempAngle < 30 :
			angl += tempAngle
			count += 1
		elif tempAngle > 330 :
			angl -= (360 - tempAngle)
			count += 1

	# displayImg(dispLines, "lines")
	# destroyImg("lines")
	if count != 0:
		angl /= count
	# print "Angle : %d Count : %d " % (angl, count)
	return angl, status


def rotate_image(image, angl):
	" Rotates the given image by given angle. (positive angle = counter clockwise rotation) "
	if angl == 0:
		return image
	imgHeight, imgWidth = image.shape[:2]
	rotated = image
	rotMat = cv2.getRotationMatrix2D( (int(image.shape[1]/2), int(image.shape[0]/2)), angl, SCALE_FACTOR )
	
	if (angl == 90 or angl == -90) :
		cv2.warpAffine(image, rotMat, (imgWidth, imgHeight), rotated, cv2.INTER_LINEAR)
	else :
		cv2.warpAffine(image, rotMat, (imgWidth, imgHeight), rotated, cv2.INTER_CUBIC)

	return rotated





# fin = cv2.imread(sys.argv[1], cv2.IMREAD_GRAYSCALE)
# templateImgC = cv2.imread(sys.argv[1], cv2.IMREAD_COLOR)
# templateImg = cv2.cvtColor(templateImgC,cv2.COLOR_BGR2GRAY)



# angle, status = compute_skew(templateImg)
# print angle
# if status:
# 	rot = rotate_image(templateImgC, angle)
# displayImg(rot, "rotated")
# destroyImg("rotated")
# cv2.imwrite('./exp_pics/temp3.jpg', templateImgC)