#!/usr/bin/env python

import cv2
import sys
import numpy as np
import json
import copy
from collections import *
from db_op import *
from desk import *
from binarization import *

COLOR_BLACK = 0
COLOR_WHITE = 255

TOL_WHITE_SMEAR = 0.5
TOL_BLACK_SMEAR = 0.8

TOL_HEIGHT_MIN = 0.5
TOL_HEIGHT_MAX = 2.0

TOL_WIDTH_MIN = 3.0
TOL_WIDTH_MAX = 20.0
FX_FACT = 770.00
FY_FACT = 1070.00


# IMG_FACT is used to determine the ROI in the image to be searched for respective field
IMG_FACT = 20 

def displayImg(image, name):
	" Displays 'image' in the window with 'name' "
	cv2.namedWindow(name, cv2.WINDOW_AUTOSIZE)
	# disp = (COLOR_WHITE - image) #to invert image
	cv2.imshow(name, image)


def destroyImg(name):
	" Destroys image window with 'name' "	
	while (cv2.waitKey() & 0xff) != ord('q'): pass
	cv2.destroyAllWindows()
	# if cv2.waitKey(0) == ord('a'):
	# 	cv2.destroyWindow(name)


def sortFields(cords):
	# print "c before : ", cords
	cord = copy.deepcopy(cords)
	for i,c in enumerate(cord):
		c.append(i+1)
	
	sc = sorted(cord, key=lambda s:s[1])
	order = [x[4] for x in sc]
	# print "order : ", order
	return order
	

def read_TemplateMaster(formID, formPageNo):
	" performs db read to get field cordinates and returns cordinates as well as all the rows "
	cur = db_readTemplateMaster(formID, formPageNo)
	cur = map(list, cur)
	# print "template master read : ", cur
	fieldIDs, cord, fieldType = [], [], []
	# field_id, field_cord, field_type
	for row in cur:
		if row[1] != "none" :
			if row[3] is 1:
				# print "hello : ",type(row[1])
				cord.append(map(int, json.loads(row[1])["rec"]))
			else :
				cord.append(map(int, map(float, row[1].split(','))))
			
			fieldIDs.append(row[0])
			fieldType.append(row[2])
			# cord.append([int(float(row[1].split(',')[0])), int(float(row[1].split(',')[1])), \
		 # 		int(float(row[1].split(',')[2])), int(float(row[1].split(',')[3])) ])
	# sort field about Yaxis
	order = sortFields(cord)
	# print "exiting : ", (cord, fieldIDs, fieldType, order)
	# exit()
	return cord, fieldIDs, fieldType, order 


def write_blobs(formID, formPageNo, fieldIDs, sel_blob, order):
	" performs db write. appends selected blobs for each cordinates \
		in rows initially read and sends them for writing "
	
	count = 0
	rows = []

	for fid, blobs, orf in zip(fieldIDs, sel_blob, order):
		container = []
		temp = json.dumps(blobs)
		container.append(formID)
		container.append(formPageNo)
		container.append(fid)
		container.append(orf)
		container.append(temp)
		rows.append(container)	

	rows = map(tuple,rows)	

	# print "rows to be written : ", rows	
	result = db_writeCords(rows)
	return result


def horizontal_smearing(image, color, tolorance):
	" smears row-wise to given color(COLOR_WHITE or COLOR_BLACK ) with tolorance " 
	start = -1
	length = 0
	height = image.shape[0]
	width = image.shape[1]

	for i in range(height):
		start = -1
		length = 0
		for j in range(width):
			if image.item(i, j) != color:
				length += 1
				if start == -1 :
					start = j
			else:
				if length <= tolorance and length != 0:
					for k in range(0, length):
						if start+k < width:
							image.itemset((i, start +k), color)
					length = 0
					start = -1		
				else:
					length = 0
					start = -1
	

def vertical_smearing(image, color, tolorance):
	" smears column-wise to given color(255=white, 0=black) with tolorance" 
	# print "tolorance : ", tolorance
	start = -1
	length = 0
	height = image.shape[0]
	width = image.shape[1]

	for i in range(width):
		start = -1
		length = 0
		for j in range(height):
			if image.item(j, i) != color:
				length += 1
				if start == -1:
					start = j
			else:
				if length <= tolorance and length != 0:
					for k in range(0, length):
						if start+k < height:
							image.itemset((start +k, i), color)
					length = 0
					start = -1
				else:
					length = 0
					start = -1


def rectangleResize(cords, tol, boundingCords):

	# USAGE : rectangleResize(c,0.20,[0,0,img.shape[1],img.shape[0]])

	w = (cords[2]-cords[0])
	h = (cords[3]-cords[1])
	cords[0] = int(cords[0] - (w*tol/2))
	if cords[0]<boundingCords[0]:
		cords[0] = boundingCords[0]
	cords[2] = int(cords[2] + (w*tol/2))
	if cords[2]>boundingCords[2]:
		cords[2] = boundingCords[2]

	cords[1] = int(cords[1] - (h*tol/2))
	if cords[1]<boundingCords[1]:
		cords[1]=boundingCords[1]
	
	cords[3] = int(cords[3] + (h*tol/3))
	if cords[3]>boundingCords[3]:
		cords[3] = boundingCords[3]



def defineSearchROI(cords_copy, imgHeight, imgWidth):
	""" Defining "Search-Area" for Fields """
	
	h = int(float((imgHeight / IMG_FACT)))
	# the factor IMG_FACT is observational.
	
	for c in cords_copy:
		# setting width to page width
		c[0] = 0
		c[2] = imgWidth -1	
		# setting height of field as per image height
		if (c[1] - h ) > 0:
			c[1] = c[1] - h
		else:
			c[1] = 0	

		if (c[3] + h ) < imgHeight :
			c[3] = c[3] + h
		else:
			c[3] = imgHeight - 1



def selectBlobs(image, cords_copy, cords, char):
	""" Recieves a preprocessed Image and select blobs to uniquely identify fields """

	
	# for c in cords_copy:
		
	# 	searchROI = image[c[1]:c[3], c[0]:c[2]]
	# 	# tempROI = imgC[areaCord[1]:areaCord[3], areaCord[0]:areaCord[2]]
	# 	print "Search ROI : ", (c[1], c[3], searchROI.shape[:2] )
	# 	displayImg(searchROI, "searchROI")	
	# 	destroyImg("searchROI")
	# 	continue
	# exit()	
	# Inversing image before finding contours
	# prepCopy1 = image.copy()
	image = 255 - image
	# displayImg(image, "imageCopy")	

	contours, hier = cv2.findContours(image, cv2.RETR_LIST, cv2.CHAIN_APPROX_SIMPLE)

	rectangles = []
	for cnt in contours :
		x,y,w,h = cv2.boundingRect(cnt)
		if (((TOL_WIDTH_MIN*char[1]) < w ) and ( w < (TOL_WIDTH_MAX*char[1])) \
			and ((TOL_HEIGHT_MIN*char[0]) < h) and (h < (TOL_HEIGHT_MAX*char[0])) ) :
			rectangles.append([x, y, x+w, y+h])
			# print "width : %d, height = %d" % (w,h)
			# cv2.rectangle(prepCopy1, (x, y),(x +w,y+h), COLOR_BLACK)
			# displayImg(prepCopy1, "imageCopy")	
			# cv2.waitKey(0)
	
	# Selecting "blobs by index" lying in "Search-Area" of Field
	sel_blob = []
	# print "rectangles : ", len(rectangles)
	# areaCord = []
	for c in cords_copy:
		temp2 = []
		# print "New Fields :  "
		for r in rectangles:
			if r[0]>=c[0] and r[1]>=c[1] and r[2]<=c[2] and r[3]<=c[3]:
				# temp.append(rectangles.index(r))
				temp2.append(r)
			
		# print " printing blobs per field : ", temp2	
		sel_blob.append(temp2)
		# print "sel_blob : ", sel_blob
		# areaCord.append(temp)		

	# Arranging blobs in the order by least zeros in the blob
	# for each,c in zip(areaCord, cords):
	# 	temp = []
	# 	for i in each:
	# 		blb = rectangles[i]
	# 		roi = image[blb[1]:blb[3], blb[0]:blb[2]]
	# 		temp.append([np.count_nonzero(roi), blb])
		
	# 	temp.sort()
	# 	temp.reverse()
	# 	sel_blob.append(calBestBlobs(temp, c))
	# print "select blobs ", sel_blob 
	return sel_blob



def prepImage(image, char):
	""" Preprocess image by smearing and masking fields marked by user. """
	# print "char ", char
	horizontal_smearing(image, COLOR_BLACK, int(TOL_BLACK_SMEAR*char[1]))  # horizontal black 
	# displayImg(image, "horizontal black")
	vertical_smearing(image, COLOR_WHITE, int(TOL_WHITE_SMEAR*char[0]))    # vertical white
	# displayImg(image, "vertical white")
	# destroyImg("vertical white")
	# exit()




def calCharWidth(image):
	""" Calculates Width and Height of Characters in Given Form by drawing Contours
	     and taking average of the two most occuring widths and heights """

	imgH = image.shape[0]
	image = image[(imgH*0.25): (imgH - imgH*0.25)]
	# Inversing Image befor contour detection
	imageCpy = image.copy()
	image = 255-image
	contour, heir = cv2.findContours(image, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)
	dicW = defaultdict(int)
	dicH = defaultdict(int)
	for cnt in contour:
		x,y,w,h = cv2.boundingRect(cnt)
		if w < 3 or h <= 3:
			continue
		dicW[w] += 1
		dicH[h] += 1
		# print "width : %d, height = %d" % (w,h)
		# cv2.rectangle(imageCpy, (x, y),(x +w,y+h), COLOR_BLACK)
		# displayImg(imageCpy, "imageCopy")	
		# cv2.waitKey(0)

	sortW = sorted(dicW, key=dicW.__getitem__, reverse=True)
	sortH = sorted(dicH, key=dicH.__getitem__, reverse=True)

	# print "Sorted Widths : ", sortW[:10]
	# print "Sorted Heights : ", sortH[:10]
	# for x,y in zip(sortW, sortH):
	# print " count  : ", dicW
	# print "Y count : ", dicH
	sumW, sumN = 0,0
	for w in sortW[:3] :
		sumW += (dicW[w]*w)
		sumN += dicW[w]

	charWidth = int(sumW / sumN)

	sumH, sumN = 0,0
	for h in sortH[:3] :
		sumH += (dicH[h]*h)
		sumN += dicH[h]

	charHeight = int(sumH / sumN)

	# print "****** Char Width is =  ", charWidth
	# print "****** Char Height is =  ", charHeight

	return [charHeight, charWidth]



def calFXFY(imgHeight, imgWidth):
	fx = 1.0;
	fy = 1.0;
	if imgWidth > FX_FACT:
		fx = float(FX_FACT) / float(imgWidth)

	if imgHeight > FY_FACT:
		fy = float(FY_FACT)/float(imgHeight)

	return fx,fy	


		


def driverProcessTemplate(templateImgPath, formID, formPageNo):

	img = cv2.imread(templateImgPath, cv2.IMREAD_GRAYSCALE)
	imgHeight, imgWidth = img.shape[:2]
	
	cords, fieldIDs, fieldType, order = read_TemplateMaster(formID, formPageNo)
	cords_copy = copy.deepcopy(cords)

	imgGray = img.copy()
	angle, status = compute_skew(imgGray)
	if status :
		img = rotate_image(img, angle)
	
	fin = binarize(img)
	prepCopy = fin.copy()
	# Masking user selected area from template before processing
	for c,t in zip(cords, fieldType) :
		if t == "multiple" :
			continue
		cv2.rectangle(fin, (c[0], c[1]), (c[2], c[3]), COLOR_WHITE, -1)
		# print "cord : ", c

	# displayImg(fin, "binarized and masked")	
	# destroyImg("binarized and masked")
	# exit()
	imageCopy = fin.copy()
	fx,fy = calFXFY(imgHeight, imgWidth)
	# resize(src, dst, dst.size(), 0, 0, interpolation);
	# imageCopy1 = cv2.resize(imageCopy, dsize[, dst[, fx[, fy[, interpolation]]]])
	imageCopy1 = cv2.resize(imageCopy,(0,0), fx=fx, fy=fy, interpolation=cv2.INTER_CUBIC)
	# displayImg(imageCopy1, "resized and masked")	

	char1 = calCharWidth(imageCopy1)
	char = (int(char1[0]/fy), int(char1[1]/fx))
	# print "char : ", char

	prepImage(prepCopy, char)
	# exit()
	# Modifies cords_copy to the extended search region.
	defineSearchROI(cords_copy, imgHeight, imgWidth)

	sel_blob = selectBlobs(prepCopy, cords_copy, cords, char)
	# print "SEL blobs : ", sel_blob
	# for xs in sel_blob:
		# print len(xs)
		# for x in xs:
			# cv2.rectangle(img,(x[0], x[1]),(x[2],x[3]), COLOR_BLACK)
			# print x
	# displayImg(img, "smeared")
	# destroyImg("smeared")
	
	# print "here : in pixels.. "
	result = write_blobs(formID, formPageNo, fieldIDs, sel_blob, order)
	
	if result: 
		return result
	else:
		return True #, "DB write Failed !!"

# TODO: for form templates who do not have blobs.
# driverProcessTemplate("upload/1_31_1.jpg", 31, 1)
# driverProcessTemplate("scan1/image2055.bmp", 21, 1)
# /home/harsh/office/opencv-exp/heuristics/scan1/image2055.bmp


