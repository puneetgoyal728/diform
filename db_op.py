import MySQLdb
import config as conf

def db_readTemplateMaster(formID, formPageNo):
	db = MySQLdb.connect(host=conf.dbHostName, user=conf.dbUserName, passwd=conf.dbPasswd, db=conf.dbDBName)
	cur = db.cursor()
	query1 = "SELECT field_id, field_cord, field_type, istable FROM tbl_template_master WHERE form_id = '%d' AND page_no = '%d'" % (formID, formPageNo)
	cur.execute(query1)

	all = cur.fetchall()
	
	db.close()
	return all

# TODO write proper blobs
def db_writeCords(rows):
	db = MySQLdb.connect(host=conf.dbHostName, user=conf.dbUserName, passwd=conf.dbPasswd, db=conf.dbDBName)
	cur = db.cursor()
	
	for each in rows:
		query1 = "INSERT INTO tbl_cordinates \
			(form_id, page_no, field_id, field_order, blobs) \
			VALUES ('%d', '%d', '%d', '%d', '%s')" % each
		try:
			cur.execute(query1)
			db.commit()
			# print "Writing Blobs peacefully :)"
		except:
			# print "here"
			return False
			db.rollback()

	db.commit()
	db.close()
	return True


# TODO select only two columns, fieldCords, and blobs
def db_readTemplate(formID, formPageNo):
	db = MySQLdb.connect(host=conf.dbHostName, user=conf.dbUserName, passwd=conf.dbPasswd, db=conf.dbDBName)
	cur = db.cursor()
	# query1 = "SELECT field_id, blobs FROM tbl_cordinates WHERE form_id = '13' and page_no='1'"
	query1 = "SELECT tbl_template_master.field_id, tbl_template_master.field_cord, \
				tbl_cordinates.blobs, tbl_template_master.istable \
				FROM tbl_template_master INNER JOIN tbl_cordinates \
				ON tbl_template_master.field_id = tbl_cordinates.field_id AND tbl_template_master.form_id = tbl_cordinates.form_id \
				AND tbl_template_master.page_no = tbl_cordinates.page_no WHERE tbl_cordinates.form_id ='%d'\
				AND tbl_cordinates.page_no ='%d' ORDER BY tbl_cordinates.field_order;" % (formID, formPageNo)
	cur.execute(query1)

	all = cur.fetchall()
	
	db.close()
	return all


def db_read_template(formID, formPageNo):
	db = MySQLdb.connect(host=conf.dbHostName, user=conf.dbUserName, passwd=conf.dbPasswd, db=conf.dbDBName)
	cur = db.cursor()
	# query1 = "SELECT field_id, blobs FROM tbl_cordinates WHERE form_id = '13' and page_no='1'"
	query1 = "SELECT field_id, field_cord FROM tbl_template_master WHERE form_id ='%d' AND page_no ='%d';" % (formID, formPageNo)
	cur.execute(query1)

	all = cur.fetchall()
	
	db.close()
	return all	


def writeSnippetMaster(row):
	db = MySQLdb.connect(host=conf.dbHostName, user=conf.dbUserName, passwd=conf.dbPasswd, db=conf.dbDBName)
	cur = db.cursor()
	curFetch = db.cursor()
	
	query1 = "INSERT INTO tbl_snippets_master \
		(job_id, page_no, field_id, cell_id, processed) \
		VALUES ('%d', '%d', '%d', '%s', '%d')" % row
	try:
	   cur.execute(query1)
	   db.commit()
	except:
		# print "did not write to DB: ********************************************************************************************"
		db.rollback()

	db.commit()

	query2 = "SELECT tsm_id from tbl_snippets_master where tsm_id = LAST_INSERT_ID()"	

	curFetch.execute(query2)
	all = curFetch.fetchall()
	db.close()

	return all
		
	
def readJobMaster(jobID):
	db = MySQLdb.connect(host=conf.dbHostName, user=conf.dbUserName, passwd=conf.dbPasswd, db=conf.dbDBName)
	cur = db.cursor()
	query1 = "SELECT no_pages, form_id FROM tbl_job_master WHERE active = '1' AND job_id = '%d'" % (jobID)
	cur.execute(query1)

	all = cur.fetchall()
	
	db.close()
	return all



def readJobDetails(jobID):
	db = MySQLdb.connect(host=conf.dbHostName, user=conf.dbUserName, passwd=conf.dbPasswd, db=conf.dbDBName)
	cur = db.cursor()
	query1 = "SELECT page_no, saved_name, form_page_no FROM tbl_job_details WHERE processed = '0' AND active = '1' AND job_id = '%d'" % (jobID)
	cur.execute(query1)

	all = cur.fetchall()
	# print "here in readJobDetails and JobID  :  ", (len(all), jobID)
	db.close()
	return all


def updateJobDetails(jobID, formID, PageNo):
	db = MySQLdb.connect(host=conf.dbHostName, user=conf.dbUserName, passwd=conf.dbPasswd, db=conf.dbDBName)
	cur = db.cursor()
	
	query1 = "UPDATE tbl_job_details SET processed = '1' \
			WHERE job_id = '%d' AND form_id = '%d' AND page_no = '%d'" % (jobID, formID, PageNo)
	try:
	   cur.execute(query1)
	   db.commit()
	except:
		# print "did not write to DB: ********************************************************************************************"
		db.rollback()

	db.commit()
	db.close()


def readImgMaster(formID):
	db = MySQLdb.connect(host=conf.dbHostName, user=conf.dbUserName, passwd=conf.dbPasswd, db=conf.dbDBName)
	cur = db.cursor()
	query1 = "SELECT page_no, data_img_name, processed FROM tbl_image_master WHERE active = '1' AND form_id = '%d'" % (formID)
	cur.execute(query1)

	all = cur.fetchall()
	# print "here in readImgMaster and formID  :  ", (len(all), formID)
	db.close()
	return all


def updateImgMaster(formID, formPageNo):
	db = MySQLdb.connect(host=conf.dbHostName, user=conf.dbUserName, passwd=conf.dbPasswd, db=conf.dbDBName)
	cur = db.cursor()
	
	query1 = "UPDATE tbl_image_master SET processed = '1' \
			WHERE page_no = '%d' AND form_id = '%d'" % (formPageNo, formID)
	try:
		cur.execute(query1)
		db.commit()
		# print "after updateing IMAGE ImgMaster : "
	except:
		# print "did not write to DB: ********************************************************************************************"
		db.rollback()

	db.commit()
	db.close()	
	