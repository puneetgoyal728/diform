""" binarize the image provided as argument and save it. """

import cv2
import sys
import numpy as np
import pybinarymodule


def binarize(image):
	myobj = pybinarymodule.Binarize("something")
	processedImage = myobj.adaptiveBinarize(image, 'i') 
	return processedImage













# img = cv2.imread(sys.argv[1], 0)

# dst = cv2.adaptiveThreshold(img, 255, cv2.ADAPTIVE_THRESH_GAUSSIAN_C, cv2.THRESH_BINARY, 3, 6)

# img_array = np.array(dst)



# cv2.namedWindow('image', cv2.WINDOW_AUTOSIZE)
# cv2.imshow('image', dst)
# cv2.imwrite('./exp_pics/binarized.JPG', dst)
# cv2.waitKey(0)
# cv2.destroyWindow('image')	
