#!/usr/bin/env python

import cv2
import sys
import numpy as np
import itertools
import math
import json
import copy
# import time
# import profile
# from memprof import memprof
# from guppy import hpy
import os
import operator
import array
from common import anorm, getsize
from binarization import *
from db_op import *
from desk import *
from orb import scaleImageORB

COLOR_BLACK = 0
COLOR_WHITE = 255

# IMG_FACT is used to determine the ROI in the image to be searched for respective field
IMG_FACT = 13 

# Extensins of blobs to improve the accuracy of template matching.
HOR_EXT = 7
VER_EXT = 2

# Offsets are used to determine offset of a field in INPUT from TEMPLATE, so it can be used to determine fields whose blobs dont match.
OFFSET = [None, None]
GLOBAL_SHIFT = 0

GLOBAL_OFFSET = [0,0]

#  ORI_PORTRAIT = true for Portrait and False for Landscape
ORI_PORTRAIT = True 

SNIPPET_SCALE_RATIO = 1
SNIPPET_PADDING_MIN = 20
SNIPPET_PADDING_MAX = 40
# Color palette Blue-Green-Red
BGR = [[255,0,0], [0, 255, 0], [0, 0, 255], [250, 100, 0], [0, 250, 100], [100, 0, 250], [80,150,220]]

# Image width
# PIX_TOL_FACTOR = 75
# PIX_TOL = 10
# toloernance between blobs to be selected after matching
# BLB_TOL = 5

# This is to store the blobs uniquely and mark plotted before remarking them.
GLOBAL_BLOB_LIST = {}
GLOBAL_MATCH_BLOB = []


class FastResetDict(object):

    def __init__(self, value_type):
        self._key_to_index = {}
        self._value_type = value_type
        self._values = array.array(value_type)

    def __getitem__(self, key):
        return self._values[self._key_to_index[key]]

    def __setitem__(self, key, value):
        self._values.append(value)
        self._key_to_index[key] = len(self._values) - 1

    def reset_content_to_zero(self):
        zero_string = '\x00' * self._values.itemsize * len(self._values)
        self._values = array.array(self._value_type, zero_string)



def defineSnippetScale(imgHeight, imgWidth):
	#  for portrait
	MIN_FX = 1200.00
	ratio = 1.0
	if imgWidth < imgHeight:
		if imgWidth > MIN_FX:
			ratio = MIN_FX/imgWidth
	else :
		if imgHeight > MIN_FX:
			ratio = MIN_FX/imgHeight
	
	return ratio;

def defineSnippetPadding(imgWidth):
	global SNIPPET_PADDING_MAX, SNIPPET_PADDING_MIN
	MIN_PAD = 10
	min_pad = int(imgWidth*0.01)
	max_pad = int(imgWidth*0.017)
	if min_pad < MIN_PAD:
		min_pad = MIN_PAD
		max_pad = 2*MIN_PAD	

	SNIPPET_PADDING_MIN = min_pad
	SNIPPET_PADDING_MAX = max_pad	



def setGlobalVar(imgHeight, imgWidth):
	# Offsets are used to determine offset of a field in INPUT from TEMPLATE, so it can be used to determine fields whose blobs dont match.
	global GLOBAL_SHIFT, OFFSET, GLOBAL_OFFSET, GLOBAL_BLOB_LIST, GLOBAL_MATCH_BLOB, SNIPPET_SCALE_RATIO

	OFFSET = [None, None]
	GLOBAL_SHIFT = 0
	# print "Reseting Global variables : ", GLOBAL_SHIFT
	# SHIFT_FACTOR = 0.35
	GLOBAL_OFFSET = [0,0]
	
	# This is to store the blobs uniquely and mark plotted before remarking them.
	GLOBAL_BLOB_LIST = FastResetDict('i')
	GLOBAL_MATCH_BLOB = []

	#  ORI_PORTRAIT = true for Portrait and False for Landscape
	ORI_PORTRAIT = True 

	SNIPPET_SCALE_RATIO = defineSnippetScale(imgHeight, imgWidth)
	defineSnippetPadding(imgWidth)

	# PIX_TOL = 10
	# BLB_TOL = 5


def displayImg(image, name):
	" Displays 'image' in the window with 'name' "
	cv2.namedWindow(name, cv2.WINDOW_AUTOSIZE)
	cv2.imshow(name, image)


def destroyImg(name):
	" Destroys image window with 'name' "	
	while (cv2.waitKey() & 0xff) != ord('q'): pass
	cv2.destroyAllWindows()


# Uses functions from module orb
# @memprof(plot = True)
def readImg(filePath, template, kp2, desc2):
	" Returns image at filePath after applying heuristics on it. "
	# Reading in GRAY_SCALE, as binarization needs to be done before computing skew angle.
	try:
		imgTemp = cv2.imread(filePath, cv2.IMREAD_COLOR)
		imgTemp.shape 
	except AttributeError:
		return [], False
	# TODO use same GRAY copy of imgTemp and return it so as to get one in the main code.
	# operateOn = cv2.imread(filePath, cv2.IMREAD_GRAYSCALE)
	operateOn = cv2.cvtColor (imgTemp,cv2.COLOR_BGR2GRAY)
	
	# Passing both images in GRAYSCALE
	transformH, flagAffine = scaleImageORB(operateOn, template, kp2, desc2)
	if flagAffine:
		if transformH is not None:
			# print "Transformation applied : ", transformH
			imgTemp = cv2.warpAffine(imgTemp, transformH, (template.shape[1],template.shape[0]))
	else :
		imgTemp = cv2.warpPerspective(imgTemp, transformH, (template.shape[1],template.shape[0]))

		
	# displayImg(template, "template")
	# displayImg(imgTemp, "input")
	# destroyImg("input")	
	return imgTemp, True

# @memprof
def fillGlobalBlobList(blobs):

	global GLOBAL_BLOB_LIST
	# print "blobs  ", blobs
	for blob in blobs :
		for b in blob :
			# print b
			GLOBAL_BLOB_LIST[str(b)] = 0

# @memprof(plot = True)
def get_cords(formID, formPageNo):
	" performs db read to get field cordinates and returns cordinates as well as all the rows "
# TODO make a try except for DB read Fail.
	cur = db_readTemplate(formID, formPageNo)
	cur = map(list, cur)
	blobs = []
	fieldCords = []
	fieldID = []
	isTable = []
	# print cur
	for row in cur:
		# field cordinates
		if row[3] is 1:
			fieldCords.append(json.loads(row[1]))
		else :
			fieldCords.append(map(int, map(float, row[1].split(','))))
		
		fieldID.append(row[0])
		blobs.append(json.loads(row[2]))
		isTable.append(row[3])
	
	# print "total rows : ", len(cur)
	fillGlobalBlobList(blobs)
	# print "exiting : ", ( fieldCords, fieldID, isTable)
	# exit()

	return blobs, fieldCords, fieldID, isTable


def get_new_cords(formID, formPageNo):
	" performs db read to get field cordinates and returns cordinates as well as all the rows "

	cur = db_read_template(formID, formPageNo)
	cur = map(list, cur)
	# blobs = []
	fieldCords = []
	fieldID = []
	# print cur
	for row in cur:
		# field cordinates
		fieldID.append(row[0])
		fieldCords.append(map(int, map(float, row[1].split(','))))
		# blobs.append(json.loads(row[4]))
	
	# print "total rows : ", len(cur)
	# exit();

	return fieldCords, fieldID	


def field_roi(fieldCord, imgHeight, imgWidth):
	" Determines the region-of-interest to search within, for a given field "
	
	Err = False
	h = int(float((imgHeight / IMG_FACT)))
	
	shift = GLOBAL_SHIFT
	# print "shift = ", shift
	# setting width to page width 
	fieldCord[0] = 0
	fieldCord[2] = imgWidth -1	
	# setting height of field as per image height
	if (fieldCord[1] - h + shift) > 0:
		if (fieldCord[1] - h + shift) < imgHeight-(2*h):
			fieldCord[1] = fieldCord[1] - h + shift
		else :
			Err = True
	else:
		fieldCord[1] = 0	

	if (fieldCord[3] + h + shift) < imgHeight :
		if (fieldCord[3] + h + shift) > 2*h:
			fieldCord[3] = fieldCord[3] + h + shift
		else :
			Err = True	
	else:
		fieldCord[3] = imgHeight - 1

	GLOBAL_OFFSET[1] = shift			
	return fieldCord, Err


def safeArea(point, field, sel):
	" Returns true, if the point lies in field boundry and is safe. "
	if sel:
		add = 0
	else :
		add = GLOBAL_OFFSET[1]
		
	if point[1]+ add >= field[1] and point[3]+ add <= field[3] :
		return True
	else :
		return False
		

def explore_match(win, img1, img2, kp_pairs, status = None, H = None):
    h1, w1 = img1.shape[:2]
    h2, w2 = img2.shape[:2]
    vis = np.zeros((max(h1, h2), w1+w2), np.uint8)
    vis[:h1, :w1] = img1
    vis[:h2, w1:w1+w2] = img2
    vis = cv2.cvtColor(vis, cv2.COLOR_GRAY2BGR)

    if H is not None:
        corners = np.float32([[0, 0], [w1, 0], [w1, h1], [0, h1]])
        corners = np.int32( cv2.perspectiveTransform(corners.reshape(1, -1, 2), H).reshape(-1, 2) + (w1, 0) )
        cv2.polylines(vis, [corners], True, (255, 255, 255))

    if status is None:
        status = np.ones(len(kp_pairs), np.bool_)
    p1 = np.int32([kpp[0] for kpp in kp_pairs])
    p2 = np.int32([kpp[1] for kpp in kp_pairs]) + (w1, 0)

    green = (0, 255, 0)
    red = (0, 0, 255)
    white = (255, 255, 255)
    kp_color = (51, 103, 236)
    for xx,yy, inlier in zip(p1, p2, status):
        (x1, y1) = xx[:2]  
        (x2, y2) = yy[:2]
        if inlier:
            col = green
            cv2.circle(vis, (x1, y1), 2, col, -1)
            cv2.circle(vis, (x2, y2), 2, col, -1)
        else:
            col = red
            r = 2
            thickness = 3
            cv2.line(vis, (x1-r, y1-r), (x1+r, y1+r), col, thickness)
            cv2.line(vis, (x1-r, y1+r), (x1+r, y1-r), col, thickness)
            cv2.line(vis, (x2-r, y2-r), (x2+r, y2+r), col, thickness)
            cv2.line(vis, (x2-r, y2+r), (x2+r, y2-r), col, thickness)
    vis0 = vis.copy()
    for (x1, y1), (x2, y2), inlier in zip(p1, p2, status):
        if inlier:
            cv2.line(vis, (x1, y1), (x2, y2), green)

    cv2.imshow(win, vis)
    def onmouse(event, x, y, flags, param):
        cur_vis = vis
        if flags & cv2.EVENT_FLAG_LBUTTON:
            cur_vis = vis0.copy()
            r = 8
            m = (anorm(p1 - (x, y)) < r) | (anorm(p2 - (x, y)) < r)
            idxs = np.where(m)[0]
            kp1s, kp2s = [], []
            for i in idxs:
                 (x1, y1), (x2, y2) = p1[i], p2[i]
                 col = (red, green)[status[i]]
                 cv2.line(cur_vis, (x1, y1), (x2, y2), col)
                 kp1, kp2 = kp_pairs[i]
                 kp1s.append(kp1)
                 kp2s.append(kp2)
            cur_vis = cv2.drawKeypoints(cur_vis, kp1s, flags=4, color=kp_color)
            cur_vis[:,w1:] = cv2.drawKeypoints(cur_vis[:,w1:], kp2s, flags=4, color=kp_color)

        cv2.imshow(win, cur_vis)
    cv2.setMouseCallback(win, onmouse)
    return vis



# @memprof
def plottingBlobs(blobs, areaCord, imgC, templateImg, templateFin, searchROI, shiftH, fc):
	
	# print " ====================== plotting points ================= "
	notSafeBlob, newBlobs = [], []
	# inde=0
	formatBlob, formatNewBlob = [], []
	for b in blobs:

		blb = [b[0]-HOR_EXT, b[1]-VER_EXT, b[2]+HOR_EXT, b[3]+VER_EXT]
		
		if GLOBAL_BLOB_LIST._values[GLOBAL_BLOB_LIST._key_to_index[str(b)]] != 0:
			tempNB = GLOBAL_MATCH_BLOB[GLOBAL_BLOB_LIST._values[GLOBAL_BLOB_LIST._key_to_index[str(b)]]]

			if safeArea(blb, areaCord, False) and safeArea(tempNB, areaCord, True):
				newBlobs.append(tempNB)
				formatBlob.append(b[:2])
				formatNewBlob.append(tempNB[:2])
				# print "using old blob :)"
			else:	
				notSafeBlob.append(b)
				# print "in if --- ", blb
				# print "removing coz ...  areaCord = = ", areaCord
			continue

		
		# print "out", blb	
		# get blb from template image
		roi = templateFin[blb[1]:blb[3], blb[0]:blb[2]]

		# mark blobs on template image
		# cv2.rectangle(templateImg, (blb[0], blb[1]), (blb[2], blb[3]), COLOR_BLACK)
		# displayImg(roi, "roi")
		(height, width) = roi.shape[:2]

		# find blobs i the input image within marked area.
		result = cv2.matchTemplate(searchROI, roi, cv2.TM_CCOEFF_NORMED)
		(_, _, minLoc, maxLoc) = cv2.minMaxLoc(result)
		# cv2.rectangle(searchROI, (maxLoc[0], maxLoc[1]), (maxLoc[0]+width, maxLoc[1]+height), (BGR[inde]))
		# cv2.rectangle(imgC, (maxLoc[0], maxLoc[1]+shiftH), (maxLoc[0]+width, maxLoc[1]+height+shiftH), (255,255,255), -1)
		#  the HOR_EXT ans VER_EXT pixel are adjusted, to get the exact blob area instead of extended area
		tempNB = [maxLoc[0]+HOR_EXT, maxLoc[1]+shiftH+VER_EXT, maxLoc[0]+width-HOR_EXT, maxLoc[1]+height+shiftH-VER_EXT]
		newBlobs.append(tempNB)
		formatBlob.append(b[:2])
		# formatBlob.append(b[2:4])	
		formatNewBlob.append(tempNB[:2])
		# formatNewBlob.append(tempNB[2:4])
		# inde +=1
		# inde %= 6
		GLOBAL_MATCH_BLOB.append(tempNB)
		GLOBAL_BLOB_LIST[str(b)] = len(GLOBAL_MATCH_BLOB)-1

	# removing NOT SAFE BLOBS
	for nsb in notSafeBlob:
		blobs.remove(nsb) 
	
	oldBlobs = np.array([np.array(np.float32(x)) for x in formatBlob])
	selBlobs = np.array([np.array(np.float32(x)) for x in formatNewBlob])

	# print "Old Blobs : ", oldBlobs
	# print "New Blobs : ", selBlobs

# Warning : the value 10 is changed from 4.
	if len(oldBlobs) >= 10:
		H, status = cv2.findHomography(selBlobs, oldBlobs, cv2.RANSAC, 3.0)
		# print "H  : ", status
		# for x in H:
			# print "x :  ", x
			# print "Type ", type(x)
		# print '%d / %d  inliers/matched' % (np.sum(status), len(status))
		# print "Some lengths : ", (len(oldBlobs), len(selBlobs), len(status))
		# kp_pairs = zip(oldBlobs, selBlobs)
		# explore_match("match", templateImg, imgC, kp_pairs, status, H)
		# while (cv2.waitKey() & 0xff) != ord('q'): pass
		# cv2.destroyWindow("match")
		
		if H is not None:
			offY, offX = 0, 0
			for ob,sb,st in zip(oldBlobs, selBlobs, status):
				# print "Status : ", (st, ob, sb)
				if st[0] == 1:
					offX += sb[0]-ob[0]
					offY += sb[1]-ob[1]
					
			offX = int(offX/np.sum(status))
			offY = int(offY/np.sum(status))	
			# print "Blob Offset :  ", (offX, offY)
			
			# print "Field C : ", fc
			
			corners = np.int32(np.float32(fc) + (offX, offY, offX, offY))
			# print "corners : ", corners
			# exit()
			# H = np.delete(H,(2),axis=0)
			# a = H[0][0]
			# b = H[0][1]
			# c = H[1][0]
			# d = H[1][1]

			# angle = math.atan(c/d)
			# print "Angle : ", angle
			# sin = math.sin(angle)
			# cos = math.cos(angle)
			# H = np.array([[1, 0, offX], [0, 1, offY], [0.0, 0.0, 1.0]])
			# print "H new : ", H		
			
			# sx = (np.absolute(a)/a)*(a*a + b*b)**0.5
			# sy = (np.absolute(d)/d)*(c*c + d*d)**0.5
			# print "sx =    sy = " , (sx,sy)
			# if (np.absolute(sx) <  1/2.0) or (np.absolute(sy) < 1/2.0) or (np.absolute(sx) > 2.0) or 
			# (np.absolute(sy) > 2.0) or H[0][2] > searchROI.shape[1] /3.0 or H[1][2] > searchROI.shape[0]/3.0:
				# if OFFSET[0] is None:
				# 	print "here"	
				# 	return [],False
				# return H
			# w1 = fc[2]-fc[0]
			# h1 = fc[3]-fc[1]
			# corners = np.float32(fc)
			# corners = np.float32([[0, 0], [w1, 0], [w1, h1], [0, h1]])
			# corners = np.int32( cv2.perspectiveTransform(corners.reshape(1, -1, 2), H).reshape(-1, 2) + (fc[0], fc[1]) )
			# con = list(corners.ravel())
			# print "type of corners : ", (type(con), con)
			
			# cv2.polylines(imgC, [corners], True, (0, 255, 0))
			# displayImg(imgC, "field")
			# destroyImg("field")
			# cv2.waitKey(0)
			return corners, True

		# else :
		# Field cannot be marked
	return [], False


def calShredCords(fieldC, imgW, imgH):
	max_border = SNIPPET_PADDING_MAX
	min_border = SNIPPET_PADDING_MIN
	h = fieldC[3] - fieldC[1]
	if h > max_border :
		h = max_border
	if h < min_border :
		h = min_border
		
	x1 = fieldC[0] - 2*h
	y1 = fieldC[1] - h
	x2 = fieldC[2] + 2*h
	y2 = fieldC[3] + h
	
	if x1 < 0:
		x1 = 0
	if y1 < 0:
		y1 =0
	if x2 >= imgW:
		x2 =  imgW-1
	if y2 >= imgH:
		y2 = imgH-1

	return [x1,y1,x2,y2]		


def updateDB(fieldID, page_no, jobID, cell="NULL", processed=0):
# TODO make a try except for DB read Fail.
	row = []
	row.append(jobID)
	row.append(page_no)
	row.append(fieldID)
	row.append(cell)
	row.append(processed)
	# print "Snippet row ", row
	row = tuple(row)
	snippetNameRow = writeSnippetMaster(row)
	# print "return from db, insertIndex : ", snippetNameRow
	snippetName = map(list, snippetNameRow)
	return snippetName[0][0]


def writeImage(imgShred, snippetName, jobID):
# TODO make a try except for Img wraite Fail read Fail.
	global SNIPPET_SCALE_RATIO
	extension = ".jpg"
	filename = str(snippetName) + extension
	filePath =  conf.snippetPath + str(jobID)+ "/"

	try: 
	    os.makedirs(filePath)
	except OSError:
	    if not os.path.isdir(filePath):
	        raise

	filePath +=filename
	# cv2.imwrite(filePath, imgShred)
	dim = int(imgShred.shape[1]*SNIPPET_SCALE_RATIO), int(imgShred.shape[0]*SNIPPET_SCALE_RATIO)
	resized = cv2.resize(imgShred, dim, interpolation = cv2.INTER_AREA)
	cv2.imwrite(filePath, resized, [int(cv2.IMWRITE_JPEG_QUALITY), 60])



def shredImage(fieldC, imgStrip, fieldID, page_no, jobID):
	imgT = imgStrip.copy()
	imgOverlay = imgStrip.copy()

	cv2.rectangle(imgOverlay, (fieldC[0], fieldC[1]), (fieldC[2], fieldC[3]), (BGR[2]), 5)
	opacity = 0.35
	cv2.addWeighted(imgOverlay, opacity, imgT, 1 - opacity, 0, imgT)

	# calculate Shred cordinates
	# someCord = [fieldC[0][0], fieldC[0][1], fieldC[2][0], fieldC[2][1]]
	shred = calShredCords(fieldC, imgT.shape[1], imgT.shape[0])
	# print "shred Cords : ", shred
	imgShred = imgT[shred[1]:shred[3], shred[0]:shred[2]]

# TODO return Proper Flag .

	# displayImg(imgShred, 'Transparency')
	# cv2.waitKey(0)
	# destroyImg('Transparency')
	snippetName = updateDB(fieldID, page_no, jobID)
	writeImage(imgShred, snippetName, jobID)

	# print "DONE"
	return True


def noBlobProcess(fieldCords, fieldIDs, imgStrip, page_no, jobID, isTable):
	
	for fc, fID, isT in zip(fieldCords, fieldIDs, isTable) :
		# print "No Blobs to mark, shredding by original fields. "
		if isT is 1:
			shredFlag = shredTable(fc["row"], fc["col"], fc["rec"], imgStrip, fID, page_no, jobID)
		else :
			shredFlag = shredImage(fc, imgStrip, fID, page_no, jobID)
		if not shredFlag:
			return False, fID;
	return True, 0	


def no_Blob_Process(fieldCords, fieldIDs, imgStrip, page_no, jobID):
	
	for fc, fID in zip(fieldCords, fieldIDs) :
		# print "No Blobs to mark, shredding by original fields. "
		shredFlag = shredImage(fc, imgStrip, fID, page_no, jobID)
	if not shredFlag:
		return False, fID;
	return True, 0	



def shredTable(trow, tcol, fc, imgStrip, fieldID, page_no, jobID):
	trow = [fc[1]] + trow + [fc[3]]
	tcol = [fc[0]] + tcol + [fc[2]]
	for xi in xrange(0, len(tcol)-1):
		# generatig column cordinates of the table
		colCord = [tcol[xi]] + [trow[0]] + [tcol[xi+1]] + [trow[len(trow)-1]]
		for xj in xrange(0, len(trow)-1):
			cell_id = str(xi) + "," + str(xj)
			# print "cell :", cell_id
			corners = np.int32(np.float32([tcol[xi]] + [trow[xj]] + [tcol[xi+1]] + [trow[xj+1]]) + (OFFSET[0], OFFSET[1], OFFSET[0], OFFSET[1]))
			# shredFlag = shredImage(corners, imgStrip, fieldID, page_no, jobID)

			imgT = imgStrip.copy()
			imgOverlay = imgStrip.copy()
			cv2.rectangle(imgOverlay, (corners[0], corners[1]), (corners[2], corners[3]), (BGR[2]), -1)
			opacity = 0.35
			cv2.addWeighted(imgOverlay, opacity, imgT, 1 - opacity, 0, imgT)
			# calculate Shred cordinates
			shred = calShredCords(colCord, imgT.shape[1], imgT.shape[0])
			imgShred = imgT[shred[1]:shred[3], shred[0]:shred[2]]
			# displayImg(imgShred, 'Transparency')
			# cv2.waitKey(0)
			# destroyImg('Transparency')
			snippetName = updateDB(fieldID, page_no, jobID, cell=cell_id)
			writeImage(imgShred, snippetName, jobID)
	return True		



#  MAIN DRIVING CODE BEGINS
# Read template image in both grayscale and color
# @memprof(plot = True)
def driverMatchTemplate(templateImg, templateFin, inputImgName, formID, formPageNo, page_no, jobID, kp2, desc2):

	setGlobalVar(templateImg.shape[0], templateImg.shape[1])
	global GLOBAL_SHIFT
	
	# Reading DB to get field Cordinates.
	# fieldCords, fieldIDs = get_new_cords(formID, formPageNo)
	templateBlobs, fieldCords, fieldIDs, isTable = get_cords(formID, formPageNo)
	# if not DBFlag:
	# 	return "DB read Failed";
	# print "len of things read : ", (fieldCords, fieldIDs, isTable)
	# return True
	
	# Read Input Images in both grayscale and color	
	imgC, imgFlag = readImg(inputImgName, templateImg, kp2, desc2)
	if not imgFlag:
		return False;
	
	img = cv2.cvtColor (imgC, cv2.COLOR_BGR2GRAY)
	imgStrip = imgC.copy()

	# noBlobProcess(fieldCords, fieldIDs, imgStrip, page_no, jobID, isTable)
	# no_Blob_Process(fieldCords, fieldIDs, imgStrip, page_no, jobID)
	# return True
	# print "HERE- must not come:"

# TODO something with unprocessed fields 
	if len(GLOBAL_BLOB_LIST._values) == 0:
		unprocFid = []
		shredFlag, fID = noBlobProcess(fieldCords, fieldIDs, imgStrip, page_no, jobID, isTable)
		if not shredFlag:
			unprocFid.append(fID)
		return True
	
	# Binarization of both the images
	fin = binarize(img)
	
	newfc = []
	unmarkedField = []

	for blobs, fc, fID, isT in zip(templateBlobs, fieldCords, fieldIDs, isTable) :

		if isT is 1:
			trow = fc["row"]
			tcol = fc["col"]
			fc = fc["rec"]
		# print "---------------------------------------------asli blob : ",fc
		# print "------------ field ID------------------", (fID)
		# exit()
		blobs_copy = copy.deepcopy(blobs)

		areaCord, errVal = field_roi(fc[:], fin.shape[0], fin.shape[1])	
		if errVal:
			# TODO something with global shift and global offset
			# print "Appending fields in unmarked stack, coz Field ROI didn't work****", fID
			if isT is 1:
				unmarkedField.append([isT, fc, fID, False, trow, tcol])
			else :
				unmarkedField.append([isT, fc, fID, False])
			# break
		else :	
			searchROI = fin[areaCord[1]:areaCord[3], areaCord[0]:areaCord[2]]
			# print "Search ROI : ", (areaCord[1], areaCord[3], searchROI.shape[:2] )
			# displayImg(searchROI, "searchROI")
			# destroyImg("searchROI")
			shiftH = areaCord[1]
			# print "shiftH = ", shiftH
			
			# fin_blb, newBlobs = plottingBlobs(blobs_copy, areaCord, fin, templateImg, templateFin, searchROI, shiftH, fc)
			newfc, markFlag = plottingBlobs(blobs_copy, areaCord, fin, templateImg, templateFin, searchROI, shiftH, fc)
			
			if markFlag :
				OFFSET[1] = newfc[1] - fc[1]
				OFFSET[0] = newfc[0] - fc[0]
				GLOBAL_SHIFT = OFFSET[1]
				
				if isT is 1:
					shredFlag = shredTable(trow, tcol, fc, imgStrip, fID, page_no, jobID)
				else :
					shredFlag = shredImage(newfc, imgStrip, fID, page_no, jobID)
				# print " -----------------------------Offsets x,y : ", OFFSET	
				if not shredFlag:
					# print "Shred Write Failed"
					return False

			else :
				# print "Appending fields in unmarked stack from last else ******************************", fID
				if isT is 1:
					unmarkedField.append([isT, fc, fID, False, trow, tcol])
				else :
					unmarkedField.append([isT, fc, fID, False])


		if OFFSET[0] is not None:
			# print "Offset : ", OFFSET	
			for field in unmarkedField:
				# print "Marking field on basis of next field : "
				if field[3]:
					continue
				# print "Offset : ", OFFSET	
				fc = field[1]
				fID = field[2]
				
				if field[0] is 1:
					shredFlag = shredTable(field[4], field[5], fc, imgStrip, fID, page_no, jobID)
				else :
					tempfc = [fc[0] + OFFSET[0], fc[1] + OFFSET[1], fc[2]+OFFSET[0], fc[3]+OFFSET[1]]
					shredFlag = shredImage(tempfc, imgStrip, fID, page_no, jobID)
				if not shredFlag:
					# print "Shred Write Failed"
					return False
				
				field[3] = True
				
				# print "field cords : ", fc
				# print "in Unmarked : temp FC : ", tempfc
				# cv2.rectangle(imgC, (tempfc[0][0], tempfc[0][1]), (tempfc[0][2], tempfc[0][3]), (BGR[6]))


	# TODO Test it, if all the fields do not have sufficient blobs as per min RANSAC filter
	#		so this must be used to process the input
	if OFFSET[0] is None:
		unprocFid = []
		shredFlag, fID = noBlobProcess(fieldCords, fieldIDs, imgStrip, page_no, jobID, isTable)
		if not shredFlag:
			unprocFid.append(fID)
		return True

	return True	
	# print "all blobs : ", GLOBAL_MATCH_BLOB
	# displayImg(templateImg, "template")
	# displayImg(imgC, "input")
	# destroyImg("template")
# 	destroyImg("input")



# @memprof(plot = True)
def it_begins(inputRead):
# if __name__ == "__main__":
	inputRead = sys.argv[1]
	# from guppy import hpy

	tempImg = cv2.imread("upload/1_5_1.jpeg", cv2.IMREAD_GRAYSCALE)
	templateFin = binarize(tempImg)

	detector = cv2.SIFT()
	kp2, desc2 = detector.detectAndCompute(tempImg, None)
	# h = hpy()
	print driverMatchTemplate(tempImg, templateFin, inputRead, 5L, 1L, 1L, 310L, kp2, desc2)
	# hp = h.heap()
	# print hp
	# profile.run('print driverMatchTemplate(tempImg, templateFin, sys.argv[1], 15L, 1L, 1L, 17L, kp2, desc2); print')

# if __name__ == "__main__":
# 	it_begins(sys.argv[1])
	# profile.run('print it_begins(sys.argv[1]); print')


# driverMatchTemplate(tempImg, templateFin, 'jobs/scam form/1type/12401321.I.jpg', 1L, 1L, 1L, 1, kp2, desc2)
# driverMatchTemplate('upload/4_5_1.jpg', 'jobs/4_13_1.jpg', 5L, 1L, 1L, 13)
# (templateImg, templateFin, inputImgName, formID, formPageNo, page_no, jobID, kp2, desc2):
# /home/harsh/office/opencv-exp/heuristics/jobs/new_rotated/1_1_1.jpg
# ('upload/1_1_1.jpg', 'jobs/1_1_2.jpg', 1L, 1L, 2L, 1)

