#!/usr/bin/env python
import cv2
from binarization import *
from matchTemplate import *
from pixels import *
from db_op import *
from desk import *
import profile
import config as conf
import multiprocessing as mp
import subprocess
import copy

# fPageNos, templateImgNames, isProcessed = [],[],[]
templateImgHolder, templateBinarizedHolder, descriptorHolder = [], [], []
# formID = -1

def read_JobMaster(jobID):
	cur = readJobMaster(jobID)
	cur = map(list, cur)
	# no_pages, form_id
	# print "total rows fetched from tbl_job_master (must be one) : ", cur
	
	for row in cur:
		pageCount= row[0]
		formID = row[1]

	return formID, pageCount


def read_ImgMaster(formID):
	""" Read all template image names from tbl_image_master """
	cur = readImgMaster(formID)
	cur = map(list, cur)
	# page_no, data_image_name, processed
	# print "total rows fetched from tbl_image_master : ", cur
	# global fPageNos, templateImgNames, isProcessed
	fPageNos, templateImgNames, isProcessed = [],[],[]
	# print "here"
	for row in cur:
		fPageNos.append(row[0])
		templateImgNames.append(row[1])
		isProcessed.append(row[2])

	return fPageNos, templateImgNames, isProcessed


def read_JobDetails(jobID):
	
	cur = readJobDetails(jobID)
	cur = map(list, cur)
	# print "read_JobDetails : ", cur
	pageNos, imageNames, formPagesNo = [], [], []
	# page_no, saved_name, form_page_no
	for row in cur :
		pageNos.append(row[0])
		imageNames.append(row[1])
		formPagesNo.append(row[2])
	
	return pageNos, imageNames, formPagesNo
			



def masterTemplateProcess(formID):

	# global fPageNos, templateImgNames, isProcessed, formID
	# formID, pageCount = read_JobMaster(jobID)
	fPageNos, templateImgNames, isProcessed = read_ImgMaster(formID)
	# print "here", (fPageNos, templateImgNames, isProcessed)
	# exit()
	for fpg, timgName, status in zip(fPageNos, templateImgNames, isProcessed):
		# print "processing page number :  ", fpg
		if status is not 1:
			# process template when status is false
			# print "arguments : ", (timgName, formID, fpg)
			result = driverProcessTemplate(conf.templateParentPath + timgName, formID, fpg)
			if result:
				updateImgMaster(formID, fpg)
	return templateImgNames, fPageNos		


def bufferTemplate(templateImgNames):
#   store template for current processing of input forms.
	global templateImgHolder, templateBinarizedHolder, descriptorHolder
	detector = cv2.SIFT()
	# detector = cv2.FeatureDetector_create("SIFT")
	# surfDetector = cv2.FeatureDetector_create("SURF")

	# Creating copy of images and binarized images.
	for tempImgName in templateImgNames :
		tempImg = cv2.imread(conf.templateParentPath + tempImgName, cv2.IMREAD_GRAYSCALE)
		templateImgHolder.append(tempImg)

		imgGray = tempImg.copy()

		angle, status = compute_skew(imgGray)
		# print status
		if status :
			tempImg = rotate_image(tempImg, angle)
		# cv2.imshow("grayscale image", tempImg)
		binTempImg = binarize(tempImg)
		templateBinarizedHolder.append(binTempImg)

		# surfDescriptorExtractor = cv2.DescriptorExtractor_create("SIFT")
		# kp2 = detector.detect(tempImg)
		# (kp2, desc2) = surfDescriptorExtractor.compute(tempImg,kp2)
		
		kp2, desc2 = detector.detectAndCompute(tempImg, None)
		descriptorHolder.append([kp2, desc2])
	# print " comple"
	# exit()			


	# for formPg, imgName, pg_no in zip(formPagesNo, imageNames, pageNos):
def worker(formPg, imgName, pg_no, formID, index):
	
	# print "***************************************"
	# obtaining templateImage path from templateImgNames by mappig agaist fPageNos
	# exit();
	# index = fPageNos.index(formPg)
	
	# templateImgName = templateImgNames[index]
	templateImg = templateImgHolder[index]
	# print "before 2nd exit : "
	# exit();
	
	templateBinarized = templateBinarizedHolder[index]
	kp = descriptorHolder[index][0]
	desc = descriptorHolder[index][1]
	# TODO delete these image array after completion and check for multiple imports.

	imgName = conf.jobsPath + str(imgName)
	# cv2.imshow("binarized template", templateBinarized)
	# cv2.imshow("grayscale image", templateImg)
	# print "in worker : "
	# print (templateImgNames[index], imgName, formID, formPg, pg_no, jobID)
	# exit()
	# ('upload/4_5_1.jpg', 'jobs/4_13_9.jpg', 5L, 1L, 1L, 13)
	retVal = driverMatchTemplate(templateImg, templateBinarized, imgName, formID, formPg, pg_no, jobID, kp, desc)
	if retVal:
		updateJobDetails(jobID, formID, pg_no)
	
	code = """<?php

		include(""" + "\'"+ str(conf.crowdPopulatePath) + "\'" +""");
		echo populateTSPDforJobPage(""" + str(jobID) + """, """ + str(pg_no) + """);
	?>
	"""
	res = php(code)
	#print res
	# http://stackoverflow.com/questions/20214595/call-php-function-from-python
	


# shell execute PHP
def php(code):
	# open process
	p = subprocess.Popen(['php'], stdout=subprocess.PIPE, stdin=subprocess.PIPE, stderr=subprocess.STDOUT, close_fds=True)

	# read output
	o = p.communicate(code)[0]

	# kill process
	try:
		os.kill(p.pid, signal.SIGTERM)
	except:
		pass

	# return
	return o



if __name__ == '__main__':
	# profile.run('print masterTemplateProcess(int(sys.argv[1])); print')
	# sTime = time.time()
	# print "hello"
	jobID = int(sys.argv[1])
	formID, pageCount = read_JobMaster(jobID)
	templateImgNames, fPageNos = masterTemplateProcess(formID)
	# fPageNos, templateImgNames, isProcessed = read_ImgMaster(formID)
	# print "here : "
	# print len(templateImgNames)
	# exit()
	bufferTemplate(templateImgNames)

	pageNos, imageNames, formPagesNo = read_JobDetails(jobID)

	# print "We got :  : ", imageNames
	# exit()
	# pool = mp.Pool(processes= mp.cpu_count()-2)
	# print " hello : "

	for formPg, imgName, pg_no in zip(formPagesNo, imageNames, pageNos):

		imgName = "%d/%s" %(jobID,imgName)
		# print "In form : ", imgName
		findex = fPageNos.index(formPg)
		worker(formPg, imgName, pg_no, formID, findex)
		# pool.apply_async(worker, (formPg, imgName, pg_no, formID, copy.deepcopy(fPageNos)))

	# pool.close()
	# pool.join()
	# exit()
	
	# endTime = time.time()
	# print "Finished at : ", endTime-sTime
# http://stackoverflow.com/questions/17824607/when-i-try-to-run-the-code-it-get-the-errorpicklingerror-cant-pickle-type?rq=1
# http://stackoverflow.com/questions/20039659/python-multiprocessings-pool-process-limit
# http://stackoverflow.com/questions/15639779/what-determines-whether-different-python-processes-are-assigned-to-the-same-or-d
# http://stackoverflow.com/questions/9455645/understanding-the-usage-of-cpu-cores-of-the-multiprocessing-modul-in-python
